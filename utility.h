#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>

const char* replace(const char *input, size_t size) {
    if (size == 0) return NULL;

    char* output;
    size_t newSize = 1;
    output = malloc(5*size+1);
    output[0] = '\0';

    for (size_t i = 0; i < size; ++i) {
        if (input[i] == '&') {
            strcat(output, "&amp;");
            newSize += 5;
        }
        else if (input[i] == '<') {
            strcat(output, "&lt;");
            newSize += 4;
        }
        else if (input[i] == '>') {
            strcat(output, "&gt;");
            newSize += 4;
        }
        else {
            strncat(output, &input[i], 1);
            newSize += 1;
        }
    }

    output = realloc(output, newSize);
    return output;
}