#include "utility.h"
#include <stdio.h>

int main() {
    const char *input = "&";
    size_t size = strlen(input);

    const char *output = replace(input, size);
    printf("%s\n", output);

    free(output);

    return 0;
}