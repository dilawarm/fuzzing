#include "utility.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>

int main() {
    char *input = "&";
    char *output;
    assert(strcmp((output = replace(input, strlen(input))), "&amp;") == 0);
    free(output);

    input = "hei&på<deg>";
    assert(strcmp((output = replace(input, strlen(input))), "hei&amp;på&lt;deg&gt;") == 0);
    free(output);

    return 0;
}   